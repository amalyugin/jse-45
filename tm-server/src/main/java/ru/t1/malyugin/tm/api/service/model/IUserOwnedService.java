package ru.t1.malyugin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void add(@NotNull String userId, @NotNull M model);

    void remove(@NotNull String userId, @NotNull M model);

    void update(@NotNull String userId, @NotNull M model);

    long getSize(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@Nullable String userId);

}