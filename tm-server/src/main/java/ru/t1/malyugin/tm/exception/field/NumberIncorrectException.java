package ru.t1.malyugin.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This number '" + value + "' is incorrect...");
    }

}