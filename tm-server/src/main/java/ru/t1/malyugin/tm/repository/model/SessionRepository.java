package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.model.ISessionRepository;
import ru.t1.malyugin.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<Session> getClazz() {
        return Session.class;
    }

}