package ru.t1.malyugin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.model.ISessionRepository;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.model.ISessionService;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    public SessionService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}