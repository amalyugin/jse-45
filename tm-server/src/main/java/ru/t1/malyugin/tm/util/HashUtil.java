package ru.t1.malyugin.tm.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.component.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable ISaltProvider saltProvider,
            @Nullable final String value
    ) {
        if (saltProvider == null) return null;
        @Nullable final String secret = saltProvider.getPasswordSecret();
        @Nullable final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @Nullable
    static String salt(
            @Nullable final String value,
            @Nullable final String secret,
            @Nullable final Integer iteration
    ) {
        if (StringUtils.isBlank(value) || StringUtils.isBlank(secret) || iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (StringUtils.isBlank(value)) return null;
        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = messageDigest.digest(value.getBytes());
            @NotNull final StringBuilder stringBuilder = new StringBuilder();
            for (byte b : array) {
                stringBuilder.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return stringBuilder.toString();
        } catch (@NotNull final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}