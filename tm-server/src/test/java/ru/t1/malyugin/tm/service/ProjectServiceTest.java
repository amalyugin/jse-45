package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.User;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() {
        USER_SERVICE.removeById(user1.getId());
        USER_SERVICE.removeById(user2.getId());
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            @NotNull final Project project = new Project("P" + i, "D" + i);
            if (i <= NUMBER_OF_PROJECTS / 2) project.setUser(user1);
            else project.setUser(user2);
            PROJECT_SERVICE.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        PROJECT_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_SERVICE.getSize());

        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear(user1.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(user1.getId()));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(user2.getId()));
    }

    @Test
    public void testClearForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.clear(null));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final long actualProjectList = PROJECT_SERVICE.getSize(user1.getId());
        Assert.assertEquals(actualProjectList, PROJECT_SERVICE.getSize(user1.getId()));
        Assert.assertEquals(0, PROJECT_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.getSize(null));
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @Nullable final Project project = PROJECT_LIST.get(0);
        Assert.assertNotNull(project);
        PROJECT_SERVICE.removeById(user1.getId(), project.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(user1.getId(), project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testFindOneByIdForUser() {
        Assert.assertNull(PROJECT_SERVICE.findOneById(UNKNOWN_ID, UNKNOWN_ID));
        for (@NotNull final Project project : PROJECT_SERVICE.findAll(user1.getId()))
            Assert.assertEquals(project.getId(), PROJECT_SERVICE.findOneById(user1.getId(), project.getId()).getId());
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneById(null, UNKNOWN_ID));
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(user1.getId());
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUser().getId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedProjectList.size(), actualProjectList.size());
        for (int i = 0; i < expectedProjectList.size(); i++) {
            Assert.assertEquals(expectedProjectList.get(i).getId(), actualProjectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll((String) null));
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(user1.getId());

        PROJECT_SERVICE.changeStatusById(user1.getId(), actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_SERVICE.changeStatusById(user1.getId(), actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_STATUS).get(1).getStatus());

        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(3).getId(), "A", null);
        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(1).getId(), "B", null);

        Assert.assertEquals("A", PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_NAME).get(1).getName());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll(user1.getId());
        Assert.assertEquals(expectedProjectList.size(), PROJECT_SERVICE.findAll(user1.getId(), (Sort) null).size());
    }

    @Test
    public void testFindAllWithSortForUserForUser() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(null, Sort.BY_CREATED));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(user1.getId());
        @Nullable final Comparator<Project> comparatorByName = (Comparator<Project>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Project> comparatorByStatus = (Comparator<Project>) Sort.BY_STATUS.getComparator();

        PROJECT_SERVICE.changeStatusById(user1.getId(), actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_SERVICE.changeStatusById(user1.getId(), actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(user1.getId(), comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(user1.getId(), comparatorByStatus).get(1).getStatus());

        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(3).getId(), "A", null);
        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(1).getId(), "B", null);

        Assert.assertEquals("A", PROJECT_SERVICE.findAll(user1.getId(), comparatorByName).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(user1.getId(), comparatorByName).get(1).getName());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll(user1.getId());
        Assert.assertEquals(expectedProjectList.size(), PROJECT_SERVICE.findAll(user1.getId(), (Comparator<Project>) null).size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUserNegative() {
        @Nullable final Comparator<Project> comparatorByCreated = (Comparator<Project>) Sort.BY_CREATED.getComparator();
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(null, comparatorByCreated));
    }

    @Test
    public void testCrateProject() {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 2;
        @NotNull final Project project1 = PROJECT_SERVICE.create(user1.getId(), "NAME1", "DESCRIPTION");
        @NotNull final Project project2 = PROJECT_SERVICE.create(user2.getId(), "NAME2", null);
        PROJECT_LIST.add(project1);
        PROJECT_LIST.add(project2);

        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertEquals("NAME1", project1.getName());
        Assert.assertEquals("DESCRIPTION", project1.getDescription());
        Assert.assertEquals(user1.getId(), project1.getUser().getId());

        Assert.assertEquals("NAME2", project2.getName());
        Assert.assertEquals("", project2.getDescription());
        Assert.assertEquals(user2.getId(), project2.getUser().getId());
    }

    @Test
    public void testCrateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, null, null));
    }

    @Test
    public void testUpdateById() {
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        PROJECT_SERVICE.updateById(user1.getId(), project.getId(), "NEW N", "NEW D");
        @Nullable final Project actualProject = PROJECT_SERVICE.findOneById(user1.getId(), project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals("NEW N", actualProject.getName());
        Assert.assertEquals("NEW D", actualProject.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        PROJECT_SERVICE.changeStatusById(user1.getId(), project.getId(), Status.COMPLETED);
        @Nullable Project actualProject = PROJECT_SERVICE.findOneById(user1.getId(), project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
        PROJECT_SERVICE.changeStatusById(user1.getId(), project.getId(), null);
        actualProject = PROJECT_SERVICE.findOneById(user1.getId(), project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeStatusById(null, UNKNOWN_ID, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeStatusById(UNKNOWN_ID, null, Status.COMPLETED));
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(null, UNKNOWN_ID));
    }

}