package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.enumerated.Role;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return getEndpointLocator().getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTaskList(@Nullable final List<TaskDTO> tasks) {
        int index = 1;
        if (tasks == null) return;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void renderTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println(task);
    }

}